from django.urls import path
from . import views


urlpatterns = [
    path('', views.home, name='ydelfos-home'),
    path('about/', views.about, name='ydelfos-about'),
    path('subjects/', views.subjects, name='ydelfos-subjects'),
    path('create-exam/', views.create_exam, name='ydelfos-create-exam'),
]
