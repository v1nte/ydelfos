from django.http import HttpResponseRedirect
from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth.decorators import login_required
from .forms import ExamForm
from .models import Subject, Exam


def home(request):
    return render(request, 'ydelfos/home.html')

def about(request):
    return render(request, 'ydelfos/about.html')

@login_required
def subjects(request):

    if request.user.groups.all()[0].name == 'Students':
        query = Subject.objects.filter(students = request.user)
    else:
        query = Subject.objects.filter(professor = request.user)

    print(query)


    data = []

    for subject in query:
        exam_query = Exam.objects.filter(subject=subject)
        aux = {}

        aux['name'] = subject
        aux['dates'] = exam_query
        aux['professor'] = subject.professor
        aux['students'] = subject.students.all()

        data.append(aux)
        
    
    print(data)

    # data = {
    #     item.name : [ item.professor, item.students.all() ] for item in zip(query, exams)
    # }

    context = {
        "data": data,
    }

    return render(request, 'ydelfos/subjects.html', context)

@login_required
def create_exam(request):

    form = ExamForm()
    if request.method == 'POST':
        form = ExamForm(request.POST)
        if form.is_valid():
            form.save()
            messages.success(request, f'Examen añadido con éxito')
            return redirect('ydelfos-home')

    return render(request, 'ydelfos/create_exam.html', {'form': form})
