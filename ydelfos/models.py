from django.db import models
from django.contrib.auth.models import User


class Subject(models.Model):

    name = models.CharField(max_length=30)
    professor = models.ForeignKey(User, on_delete=models.CASCADE, related_name='PROFESOR')
    students = models.ManyToManyField(User, related_name='ALUMNOS')

    def __str__(self):

        return self.name

class Exam(models.Model):

    date = models.DateField()
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    description = models.TextField(blank=True)

    def __str__(self):

        return str(self.date)
