from django.contrib import admin
from .models import Subject, Exam


class SubjectAdmin(admin.ModelAdmin):
    list_display = ['name', 'professor']

class ExamAdmin(admin.ModelAdmin):
    list_display = ['date', 'subject']

admin.site.register(Subject, SubjectAdmin)
admin.site.register(Exam, ExamAdmin)
